<?php
 include '../plantillas/header.php';

 $cursos = $odoo->get('appstudy.cursos',null,array('name','alumnos','examenes'));
 //listar lista de cursos
 //print_r($cursos);
 ?>
  <h3 class="mt-4 mb-4">Cursos Disponibles</h3>
  <div class="row">
          <!-- /.col -->
          <?php  foreach ($cursos as $c) {  ?>    
          <div class="col-md-4">
            <!-- Widget: user widget style 1 -->
            <div class="card card-widget widget-user">
              <!-- Add the bg color to the header using any of the bg-* classes -->
              <div class="widget-user-header bg-info">
                <h3 class="widget-user-username"><?=$c['name'] ?> </h3>
                <h5 class="widget-user-desc">Curso</h5>
              </div>
              <div class="widget-user-image">
                <img class="img-circle elevation-2" src="../dist/img/user1-128x128.jpg" alt="User Avatar">
              </div>
              <div class="card-footer">
                <div class="row">
                  <div class="col-sm-4 border-right">
                    <div class="description-block">
                      <h5 class="description-header"><?=count($c['alumnos']) ?></h5>
                      <span class="description-text">Incritos</span>
                    </div>
                    <!-- /.description-block -->
                  </div>
                  <!-- /.col -->
                  <div class="col-sm-4 border-right">
                    <div class="description-block">
                      <h5 class="description-header"><?=count($c['examenes']) ?></h5>
                      <span class="description-text">Examenes</span>
                    </div>
                    <!-- /.description-block -->
                  </div>
                  <!-- /.col -->
                  <div class="col-sm-4">
                    <div class="description-block">
                      <a href="/odoo_quiz/dashboard/pages/curso.php?id=<?=$c['id']?>">
                        <button type="button" class="btn btn-block btn-default">
                             Ir al Curso
                        </button>
                      </a>
                    </div>
                    <!-- /.description-block -->
                  </div>
                  <!-- /.col -->
                </div>
                <!-- /.row -->
              </div>
            </div>
            <!-- /.widget-user -->
          </div>
          <!-- /.col -->

        <?php } ?>
          
        </div>
       
                

<?php include '../plantillas/footer.php'?>
