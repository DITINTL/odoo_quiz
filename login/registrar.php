<!DOCTYPE html>
<html lang="en">
<head>
	<title>Registro Quiz</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">

<!--===============================================================================================-->
</head>
<body>	
	<div class="limiter">
		<div class="container-login100" style="background-image: url('images/ff.png');">
			<div class="wrap-login100 p-t-30 p-b-50">
				<span class="login100-form-title p-b-41">
					Registrarse
				</span>
				<form class="login100-form validate-form p-b-33 p-t-5" action="registrar_user.php" method="POST">

                    <div class="wrap-input100 validate-input" data-validate = "Enter username">
						<input class="input100" type="text" name="name" placeholder="Escribe tu nombre de Usuario" required="">
						<span class="focus-input100 text-danger" data-placeholder="&#xe82a;"></span>
					</div>
					<div class="wrap-input100 validate-input" data-validate = "Enter username">
						<input class="input100" type="email" name="user" placeholder="Correo">
						<span class="focus-input100 text-danger" data-placeholder="&#xe82a;">
							<?=($_GET['e']==1)?'Este Correo ya esta Registrado':'' ?></span>
					</div>

					<div class="wrap-input100 validate-input" data-validate="Enter password">
						<input id="p1" class="input100" type="password" name="pass" placeholder="Escribe Contraseña">
						<span class="focus-input100" data-placeholder="&#xe80f;"></span>
					</div>
					<div class="wrap-input100 validate-input" data-validate="Enter password">
						<input id="p2" class="input100" type="password" name="pass2" placeholder="Repite Contraseña">
						<span class="focus-input100" data-placeholder="&#xe80f;" id="valid"></span>
					</div>
					<div class="container-login100-form-btn m-t-32">
						<button id="bu" class="login100-form-btn"  disabled>
							Login
						</button>
					</div>
					<a href="index.php "><p style="text-align: center">Regresar</p></a>

				</form>
				<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
                <script type="text/javascript" src="strength.js"></script>
                <script type="text/javascript" src="js.js"></script>

                <script>
$(document).ready(function($) {
	
$('#p1').strength({
            strengthClass: 'strength',
            strengthMeterClass: 'strength_meter',
            strengthButtonClass: 'button_strength',
            strengthButtonText: '',
            strengthButtonTextToggle: 'Ocultar Password'
        });

$('#p2').keyup(function() {
   if($('#p1').val()==""){
   	 $('#valid').html("<span style='color:red'> Escribe una contraseña</span>");
   	 $(this).val('');
   }else{
   	//alert($.trim($('#p2').val())+"-"+$.trim($('#p1').val()))
   	 if($.trim($('#p2').val())==$.trim($('#p1').val())){
   	 	$('#valid').html("<span style='color:green'> Contraseñas Iguales</span>");
   	 	 $('#bu').removeAttr( "disabled" );
   	 }else{
   	 		 $('#valid').html("<span style='color:red'> No coinciden las contraseñas</span>");

              $('#bu').attr( "disabled" ,'');
   	 }
   }
});

});

</script>
			</div>
		</div>
	</div>

	
     
	
     <video width="320" height="120" controls id="vi">
       <source src="/videos/videoplayback.mp4" type="video/mp4">
     </video> 
     <script type="text/javascript">
     	document.getElementById('vi').play();
     </script>

	<div id="dropDownSelect1"></div>
	
<!--===============================================================================================-->
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/daterangepicker/moment.min.js"></script>
	<script src="vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>

</body>
</html>