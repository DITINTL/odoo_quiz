<?php
 $id = $_GET['id'];
 include '../plantillas/header.php';
//session_start();
 $examen = $odoo->get('appstudy.quiz',array(
                     array('id','=', (int)$id)
                   )); 

 $preguntas = $odoo->get('appstudy.pregun',array(
                     array('quiz','=', (int)$id)
                   ),
                     array('name','respuesta','alternativas','imagen')
  ); 
 
 
 $actual = $_SESSION['pregunta_actual_<?=$id ?>'];
 if(empty($actual)){
   $actual = 0;
 }

 
 ?>
 <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6 text-center">
            <h1><?=$examen[0]['name'] ?></h1>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
 <?php foreach ($preguntas as $k => $p) { ?>
 <div class="pregunta  pr<?=$k ?>" 
  <?php if($k!=0){ ?>
     style="display: none"
  <?php } ?>
  >
 	<div  class="p-2 text-center font-weight-bold text-danger h3">
 		<?=$p['name']?>
 	</div>
 	<div class="row">
 		<div class="col">
 			<!-- obtener alternativas -->
 			<?php 
              $alt = $odoo->get('appstudy.alt',array(
                     array('pregunta','=', (int)$p['id'])
                   )
               );

              array_push($alt, array(
              	'id'=>0,
              	'name'=>$p['respuesta']
              ) );
              shuffle($alt);

              //print_r($alt);

 			 ?>
            <?php foreach ($alt as $a) { ?>
 			<button class="mb-2 alternativa btn btn-block btn-primary " data-rp="<?=$a['id'] ?>"
        data-pr="<?=$k?>">
               <!-- <i class="fas fa-search"></i> -->
                 <?=$a['name']?>
      </button>
            <?php } ?>
 		</div>
 		<?php  if($p['imagen']){?>
 		<div class="col">
 			<img class="imgcel" src="data:image/x-icon;base64,<?=$p['imagen']?>">
 		</div>
 		<?php }?>
 	</div>
 	<div class="row no-print">
                <div class="col-12">
                  <?php if ($p !== end($preguntas)) { ?>
    
                	<button type="button" class="next btn btn-primary float-right next<?=$k?>" 
                  style="margin-right: 5px;"  data-id="<?=$k?>">
                  	Siguiente
                    <i class="fas fa-arrow-circle-right"></i> 
                  </button>

                <?php } else { ?>
                  <button type="button" class="end btn btn-primary float-right" 
                  style="margin-right: 5px;"  data-id="<?=$k?>">
                    Finalizar
                    <i class="fas fa-arrow-circle-right"></i> 
                  </button>

                <?php }  ?>
                  <?php if($k!=0){ ?>

                  <button type="button" class="previous btn btn-success float-right"
                  data-id="<?=$k?>">
                  <i class="fas fa-arrow-circle-left"></i> Anterior
                  </button>

                <?php } ?>

                  
                </div>
              </div>
 </div>

 <?php } ?>

 <?php include '../plantillas/footer.php'?>
 <script src="<?=$raiz ?>dist/js/pages/examen.js"></script>