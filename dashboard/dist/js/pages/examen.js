$(document).ready(function(){
  $('.next').on('click', function(){
  	var id = $(this).attr('data-id');
  	var idnext =  parseInt(id) + 1; 
  	$('.pr'+id).hide();
  	$('.pr'+idnext).show();
  });

  $('.previous').on('click', function(){
  	var id = $(this).attr('data-id');
  	var idprevious =  parseInt(id) - 1;
  	$('.pr'+id).hide();
  	$('.pr'+idprevious).show();

  });

  $('.alternativa').on('click', function(){
  	var validar = $(this).attr("data-rp");
  	var id = $(this).attr('data-pr');

  	if (validar==0){
  		alert("correcto");
  	}else{
  		alert("Incorrecto");
  	}

  	//guardar la sesion del id actual
  	$.ajax({
  		type:"POST",
        url:"/odoo_quiz/libs/sesion_save.php",
        data:$('#f_user').serialize(),
        dataType: 'text',
        success: function(data){

        },
        error: function (xhr, ajaxOptions, thrownError) {
           alert('Ups hay un Error: '+xhr.status+'- '+thrownError);
        }
  	});

  	$('.next'+id).click();

  });

});