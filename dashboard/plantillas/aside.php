<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="/odoo_quiz/dashboard/" class="brand-link">
      <img src="/odoo_quiz/dashboard/dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">AppStudy JS</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="/odoo_quiz/dashboard/dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block"><?php print_r($_SESSION['nombre']); ?></a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item has-treeview menu-open">
            <a href="#" class="nav-link active">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Examenes
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>

            <!-- obtener la lista de cursos -->
            <?php $cursos = $odoo->get('appstudy.cursos'); ?>
            <ul class="nav nav-treeview">
              <?php  foreach ($cursos as $c) {  ?>
              <li class="nav-item">
                <a href="/odoo_quiz/dashboard/pages/curso.php?id=<?=$c['id']?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p><?=$c['name'] ?><i class="right fas fa-angle-left"></i></p>
                </a>
                 
                <ul class="nav nav-treeview">
                  <!-- obtener la lista de examenes -->
                   <?php $exa = $odoo->get('appstudy.quiz',array(
                     array('curso','=',$c['id'])
                   )); ?>

                   <?php  foreach ($exa as $e) {  ?>
                    <li class="nav-item">
                      <a href="/odoo_quiz/dashboard/pages/examen.php?id=<?=$e['id']?>" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                         <p><?=$e['name'] ?></p>
                        </a>
                    </li>
                    <?php } ?>
                </ul>

              </li>
              <?php } ?>
            </ul>


          </li>
          
          <li class="nav-item">
            <a href="/odoo_quiz/dashboard/pages/cursos.php" class="nav-link">
              <i class="nav-icon fas fa-th"></i>
              <p>
                Cursos
                <span class="right badge badge-danger">Todos</span>
              </p>
            </a>
          </li>
          
          
         
         
       
          
           
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>